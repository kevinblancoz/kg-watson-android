package com.konradgroup.kgcancer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;


import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancerActivity extends AppCompatActivity {

    static final int    REQUEST_IMAGE_CAPTURE = 1;
    private String      mCurrentPhotoPath;
    private Button      takePhoto;
    private Button      scanPhoto;
    private Button      takeOherPhoto;
    private ImageView   thumbnail;
    private Bitmap      mImageBitmap;
    private TextView    message;
    private Bitmap      scaled;
    private File        photoFile;
    private File        image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancer);

        takePhoto       = (Button) findViewById(R.id.take_photo);
        scanPhoto       = (Button) findViewById(R.id.scan_photo);
        takeOherPhoto   = (Button) findViewById(R.id.take_other);
        message         = (TextView) findViewById(R.id.message);
        thumbnail       = (ImageView) findViewById(R.id.imageView);

        //Hides some buttons inicially
        scanPhoto.setVisibility(View.GONE);
        takeOherPhoto.setVisibility(View.GONE);


        /**
         * Take photo button listener
         */
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhoto();
            }
        });

        /**
         * Take other photo button listener
         */
        takeOherPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhoto();
            }
        });

        /**
         * Scan photo button listener
         */
        scanPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            //Remove/display buttons and messages.
            takePhoto.setVisibility(View.GONE);
            message.setVisibility(View.GONE);
            scanPhoto.setVisibility(View.VISIBLE);
            takeOherPhoto.setVisibility(View.VISIBLE);

            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));

                //Scale the image
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                float scaleHt =(float) width/mImageBitmap.getWidth();
                scaled = Bitmap.createScaledBitmap(mImageBitmap, width, (int)(mImageBitmap.getWidth()*scaleHt), true);
                thumbnail.setImageBitmap(scaled);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creats a new file to the write the photo
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }



    /**
     * Method to take the photo, execute the camera intent
     */
    private void doTakePhoto() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        "com.konradgroup.kgcancer.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    /**
     * Upload the file to the API using Retrofit
     */
    private void uploadFile() {

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this,"Scanning...","Please wait...",false,false);

        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        photoFile = new File(image.getAbsolutePath());


        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), photoFile);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("images_file", photoFile.getName(), requestFile);


        // finally, execute the request
        Call<ResponseBody> call = service.upload(body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                //Dismissing the progress dialog
                loading.dismiss();
                Log.v("Upload", "success");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Dismissing the progress dialog
                loading.dismiss();
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}
